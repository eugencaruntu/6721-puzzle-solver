package entities;

public class Tile {
    private char label;
    private int value;
    private int rowIdx, colIdx;

    public Tile(char label, int rowIdx, int colIdx, int value) {
        this.label = label;
        this.rowIdx = rowIdx;
        this.colIdx = colIdx;
        this.value = value;
    }

    public Tile(Tile otherTile) {
        this(otherTile.getLabel(), otherTile.getRowIdx(), otherTile.getColIdx(), otherTile.getValue());
    }

    public char getLabel() {
        return label;
    }

    public void setLabel(char label) {
        this.label = label;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getRowIdx() {
        return rowIdx;
    }

    public int getColIdx() {
        return colIdx;
    }

    @Override
    public String toString() {
        return "{" + label +
                " =\t" + value +
//                ", weight: " + weight +
//                ", [" + rowIdx +
//                ", " + colIdx + "]" +
                '}';
    }
}
