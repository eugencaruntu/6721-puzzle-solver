package entities;

import java.util.Stack;
import java.util.StringJoiner;

public class Board {
    private int rows;
    private int columns;
    private char zeroTileLabel;
    private Tile[][] board;
    private Board parent;
    private char generatingMove = '0';
    private double weight;
    private int depth;

    /**
     * Parameterized constructor for a entities.
     * Also labels the tiles
     *
     * @param rows    the number of rows
     * @param columns the number of columns
     */
    Board(int rows, int columns, int[] values) {

        if (values.length != rows * columns) {
            System.out.println("The input lenght must be equal to " + (rows * columns));
        } else {

            this.rows = rows;
            this.columns = columns;
            this.board = new Tile[rows][columns];

            // Labeling the positions and populates the values for each time on the entities
            char label = 'a';
            label--;    // get one character ahead of a whatever that might be
            int valueIndex = 0;
            int rowIdx = 0;
            for (Tile[] row : board) {
                int colIdx = 0;
                for (int i = 0; i < row.length; i++) {
                    label++;
                    int value = values[valueIndex++];
                    row[i] = new Tile(label, rowIdx, colIdx, value);
                    if (value == 0) {
                        setZeroTileLabel(label);
                    }
                    colIdx++;
                }
                rowIdx++;
            }
        }
    }

    private Board(int rows, int columns, Tile[][] board, char zeroTileLabel) {
        this.rows = rows;
        this.columns = columns;
        this.board = new Tile[rows][columns];
        int rowIdx = 0;
        for (Tile[] row : this.board) {
            int colIdx = 0;
            for (int i = 0; i < row.length; i++) {
                row[i] = new Tile(board[rowIdx][colIdx]);
                colIdx++;
            }
            rowIdx++;
        }
        this.zeroTileLabel = zeroTileLabel;
    }

    /**
     * Default constructor for the entities. Defaults to 3 by 4 size
     */
    public Board(int[] values) {
        this(3, 4, values);
    }

    /**
     * Deep copy ctor
     * It is made private on purpose
     *
     * @param otherBoard the board to be copied
     */
    private Board(Board otherBoard) {
        this(otherBoard.getRows(), otherBoard.getColumns(), otherBoard.getBoard(), otherBoard.getZeroTileLabel());
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public Board getParent() {
        return parent;
    }

    public void setParent(Board parent) {
        this.parent = parent;
    }

    public char getZeroTileLabel() {
        return zeroTileLabel;
    }

    public void setZeroTileLabel(char label) {
        this.zeroTileLabel = label;
    }

    public Tile getByLabel(char c) {
        int pos = Character.getNumericValue(c) - 10; // a is 10
        int row = pos / columns;
        int column = pos % columns;
        return board[row][column];
    }

    public Tile getZeroTile() {
        return getByLabel(zeroTileLabel);
    }

    /**
     * Moves the value from a tile to another and sets the zeroLabelTile once move is made.
     * The move is done on a deep copy of the Board object
     *
     * @param destinationLabel the destination
     * @param weight           the weight
     * @return a new Board state after the move
     */
    public Board swapTo(char destinationLabel, int weight) {
        Board child = new Board(this);  // copy the parent entities
        child.parent = this;  // remember the parent
        child.getByLabel(zeroTileLabel).setValue(getByLabel(destinationLabel).getValue());
        child.getByLabel(destinationLabel).setValue(0);
        child.zeroTileLabel = destinationLabel;
        child.generatingMove = destinationLabel;
        child.weight = weight;
        return child;
    }

    public char getGeneratingMove() {
        return generatingMove;
    }

    /**
     * Populate the neighbors and give weight to each possible moves so that they can be used to pick the most preferred move later
     */
    public Stack<Board> getPossibleChildren() {
        Stack<Board> children = new Stack<>();
        Tile zeroTile = getZeroTile();
        char move;

        int r = zeroTile.getRowIdx();
        int c = zeroTile.getColIdx();

        // (UP)      DOWN
        if (r > 0) {
            move = board[r - 1][c].getLabel();
            children.add(swapTo(move, 0));
        }

        // (UP RIGHT)       DOWN LEFT
        if (r > 0 && c < columns - 1) {
            move = board[r - 1][c + 1].getLabel();
            children.add(swapTo(move, 1));
        }

        // (RIGHT)      LEFT
        if (c < columns - 1) {
            move = board[r][c + 1].getLabel();
            children.add(swapTo(move, 2));
        }

        // (DOWN RIGHT)     UP LEFT
        if (r < rows - 1 && c < columns - 1) {
            move = board[r + 1][c + 1].getLabel();
            children.add(swapTo(move, 3));
        }

        // (DOWN)       UP
        if (r < rows - 1) {
            move = board[r + 1][c].getLabel();
            children.add(swapTo(move, 4));
        }

        // (DOWN LEFT)      UP RIGHT
        if (r < rows - 1 && c > 0) {
            move = board[r + 1][c - 1].getLabel();
            children.add(swapTo(move, 5));
        }

        // (LEFT)       RIGHT
        if (c > 0) {
            move = board[r][c - 1].getLabel();
            children.add(swapTo(move, 6));
        }

        // (UP LEFT)        DOWN RIGHT
        if (r > 0 && c > 0) {
            move = board[r - 1][c - 1].getLabel();
            children.add(swapTo(move, 7));
        }
        return children;
    }

    /**
     * To String used in comparing states while playing the game
     * Its format is very important and changes to its implementation require revising all usage
     */
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Tile[] row : board) {
            for (Tile tile : row) {
                stringBuilder.append(tile.getValue()).append(", ");
            }
        }
        return " [" + stringBuilder.substring(0, stringBuilder.length() - 2) + "]";
    }

    /**
     * An enhanced version of toString to render the entities
     *
     * @return the entities representation
     */
    public String toStringVisual() {
        StringJoiner joiner = new StringJoiner(System.lineSeparator());
        for (Tile[] row : board) {
            StringBuilder rowBuilder = new StringBuilder();
            for (Tile tile : row) {
                rowBuilder.append(tile).append("\t\t");
            }
            joiner.add(rowBuilder);

        }
        String boardString = joiner.toString();
        return "Board: " + rows + " x " + columns + "\tZeroTile: " + zeroTileLabel +
                "\n" + boardString;
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public Tile[][] getBoard() {
        return board;
    }

    /**
     * Comparing game states based on their weights
     * Smallest weight wins
     * The weights represent the originating move, but could be used to assign other costs
     *
     * @param otherBoard the other entities to be compared against
     * @return an integer (1, 0, -1) representing the compare result
     */
    public int compareWeights(Board otherBoard) {
        if (weight > otherBoard.weight) {   // smallest weight wins
            return 1;
        } else if (weight < otherBoard.weight) {
            return -1;
        } else {    // equals
            return 0;
        }
    }

}

