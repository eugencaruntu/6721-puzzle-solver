package entities;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.IntStream;

public class Game {
    private Board startState;
    private boolean trackDepth = false;
    private String gameName;
    private String goalString;
    private Set<String> openStrings = new HashSet<>();
    private Set<String> closedStrings = new HashSet<>();
    private HashMap<String, String> searchPath = new HashMap<>();
    private Long start, duration;
    private int searchPathLenght;

    /**
     * Game constructor that sets the goal state representation
     *
     * @param rows    the number of rows
     * @param columns the number of columns
     * @param input   the input array to be played
     */
    public Game(int rows, int columns, int[] input) {
        int[] sortedInput = IntStream.of(input).sorted().toArray();
        // shift positions to left in sorted array so we can place zero at the end
        for (int i = 1; i < sortedInput.length; i++) { // first index is zero since is already sorted
            sortedInput[i - 1] = sortedInput[i];
        }
        sortedInput[sortedInput.length - 1] = 0; // put zero at the end

        startState = new Board(rows, columns, input);

        Board goalState = new Board(rows, columns, sortedInput);    // defined locally since we only care about its string representation when comparing
        goalString = goalState.toString();
    }

    /**
     * Depth First Search
     */
    public void DFS() {
        start = System.currentTimeMillis();
        gameName = "puzzleDFS.txt";
        Stack<Board> openStack = new Stack<>();
        openStack.add(startState);
        openStrings.add(startState.toString());

        while (!openStack.empty()) {
            if (playState(openStack, openStack.pop())) return; // return if playState finds a solution
        }
    }

    /**
     * The iterative deepening is combining breadth first queue data structure with the depth first stack.
     * We generate the children and add them to the queue if not yet on closed or open set
     * We poll from this queue and apply depth first up to the iterativeDepth limit. Then we add the last visited node to the open queue so that we can resume the search if needed later
     * We move to the next state polled from the queue and do depth first search up to the depth limit.
     * We redo this until we either found the goal or we exhaust the queue.
     *
     * @param iterativeDepth the limit of each iteration for which we would execute depth first
     */
    public void DFSIterativeDeepening(int iterativeDepth) {
        start = System.currentTimeMillis();
        gameName = "puzzleDFSIterativeDeepening.txt";
        trackDepth = true;
        Queue<Board> breadFirstQueue = new PriorityQueue<>(Board::compareWeights);
        breadFirstQueue.add(startState);

        Stack<Board> openStack = new Stack<>();
        openStack.add(startState);
        openStrings.add(startState.toString());

        while (!breadFirstQueue.isEmpty()) {
            Board state = breadFirstQueue.poll();
            openStack.add(state);
            determineValidChildren(breadFirstQueue, state);
            Board current;
            do {
                current = openStack.pop();
                if (playState(openStack, current)) return; // return if playState finds a solution
            } while (!openStack.empty() && current.getDepth() < iterativeDepth);

            openStack.clear();
        }
    }

    /**
     * Best First Search using any comparator as heuristic
     *
     * @param comparator the heuristic pased as argument
     */
    public void BSF(Comparator<Board> comparator) {
        start = System.currentTimeMillis();
        gameName = "puzzleBFS-" + comparator.getClass().getSimpleName() + ".txt";
        Queue<Board> openQueue = new PriorityQueue<>(comparator);
        openQueue.add(startState);
        openStrings.add(startState.toString());

        while (!openQueue.isEmpty()) {
            if (playState(openQueue, openQueue.poll())) return; // return if playState finds a solution
        }
    }

    /**
     * A* using the comparator as a function. The comparator is already combining the cost (depth) and the heuristic.
     * The depth is being recorded because the trackDepth is set tu true.
     *
     * @param comparator the function that accounts the depth cost and heuristic
     */
    public void As(Comparator<Board> comparator) {
        start = System.currentTimeMillis();
        gameName = "puzzleAs-" + comparator.getClass().getSimpleName() + ".txt";
        trackDepth = true;
        Queue<Board> openQueue = new PriorityQueue<>(comparator);
        startState.setDepth(0);
        openQueue.add(startState);
        openStrings.add(startState.toString());

        while (!openQueue.isEmpty()) {
            if (playState(openQueue, openQueue.poll())) return;  // return if playState finds a solution
        }
    }

    /**
     * Playing a current state
     * Depending on the method playing the game, the data structure can be a stack or a queue
     * If the state being played matches the goal state, we trace bach the solution path and write it to a file
     * If the state being played is not matching the goal state, we generate its children and add them to open structures
     *
     * @param open  the collection representing the data structure being played
     * @param state the current state to be played
     * @return true if the current state being played is matching the goal state, false otherwise
     */
    private boolean playState(Collection<Board> open, Board state) {
        searchPathLenght++;
        String stateString = state.toString();
        openStrings.remove(stateString);
        // keep track of parent for each child that is played
        if (state != startState) {
            searchPath.put(state.getGeneratingMove() + stateString, state.getParent().getGeneratingMove() + state.getParent().toString());
        }

        // test for goal state
        if (stateString.equals(goalString)) {
            duration = System.currentTimeMillis() - start;
            Stack<String> solutionPath = traceBackSolutionPath(state);
            writeSolutionPath(solutionPath);
            return true;
        } else {  // generate children of current state otherwise
            closedStrings.add(stateString);
            determineValidChildren(open, state);
        }

        return false;   // we did not found a solution
    }

    /**
     * Make a call to entities object to generate possible move states,
     * then if they are not part of open nor closed sets, add them to the open collection
     *
     * @param open  the collection (stack or queue) of nodes to be visited
     * @param state the state being played for which we generate and validate children
     */
    private void determineValidChildren(Collection<Board> open, Board state) {
        Stack<Board> children = state.getPossibleChildren();
        // put children on open if not already on open or closed
        while (!children.isEmpty()) {
            Board child = children.pop();
            if (trackDepth)
                child.setDepth(state.getDepth() + 1);    // if we playState A star or DFS with iterative deepening, we also increment the depth that will be used as g(n) function or to limit DFS
            String childString = child.toString();
            if (!openStrings.contains(childString) && !closedStrings.contains(childString)) {
                open.add(child);                // add state object to the open data structure
                openStrings.add(childString);   // ass the string representation to the openStrings hash set
            }
        }
    }

    /**
     * Trace back the solution path for a goal state
     * We build a stack tracing back the path up to root
     *
     * @param state the goal state
     * @return the solution path stack
     */
    private Stack<String> traceBackSolutionPath(Board state) {
        String keyChild = state.getGeneratingMove() + state.toString();

        // We build a stack so we can reverse the path when displaying it
        Stack<String> solutionPath = new Stack<>();
        solutionPath.add(keyChild);
        while (searchPath.containsKey(keyChild)) {
            solutionPath.add(searchPath.get(keyChild));
            keyChild = searchPath.get(keyChild);
        }
        return solutionPath;
    }

    /**
     * Reverse the stack to write the solution path
     *
     * @param solutionPath the stack to be printed
     */
    private void writeSolutionPath(Stack<String> solutionPath) {
//        String fileName = startState.getRows() + "x" + startState.getColumns() + "_" + gameName;
        String fileName = gameName;
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
//            writer.write("Execution time: " + duration);
//            writer.write("\tSearch path length: " + searchPathLenght);
//            writer.newLine();
            while (!solutionPath.empty()) {
                writer.write(solutionPath.pop());
                writer.newLine();
            }
            System.out.println("The solution path was saved to " + fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}