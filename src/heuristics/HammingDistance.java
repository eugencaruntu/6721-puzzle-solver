package heuristics;

import entities.Board;
import entities.Tile;

import java.util.Comparator;

public class HammingDistance implements Comparator<Board> {
    @Override
    public int compare(Board o1, Board o2) {
        if (misplacedTiles(o1) == misplacedTiles(o2)) {
            return o1.compareWeights(o2);
        } else if (misplacedTiles(o1) > misplacedTiles(o2)) {   // smaller heuristics wins
            return 1;
        } else return -1;
    }

    /**
     * Hamming distance or misplaced number of tiles
     *
     * @param board the entities to be passed as argument for which we calculate the number of misplaced tiles
     * @return the number of misplaced tiles
     */
    private static int misplacedTiles(Board board) {
        int misplaced = 0;
        int r0, c0, r1, c1;



        for (Tile[] row : board.getBoard()) {
            for (Tile tile : row) {
                int value = tile.getValue();
                if (value != 0) {
                    r0 = tile.getRowIdx();                              // current row index
                    c0 = tile.getColIdx();                              // current column index
                    r1 = (value - 1) / board.getColumns();              // goal row index
                    c1 = (value - 1) % board.getColumns();              // goal column index
                    if (r0 != r1 || c0 != c1) {
                        misplaced++;
                    }
                }
            }
        }
        return misplaced + board.getDepth(); // the depth is only used as g(n) with A*;
    }


}
