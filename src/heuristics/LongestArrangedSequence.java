package heuristics;

import entities.Board;

import java.util.Comparator;
import java.util.Scanner;

public class LongestArrangedSequence implements Comparator<Board> {
    @Override
    public int compare(Board o1, Board o2) {
        if (longestArangedSequence(o1) == longestArangedSequence(o2)) {
            return o1.compareWeights(o2);
        } else if (longestArangedSequence(o1) > longestArangedSequence(o2)) {   // smaller heuristics wins
            return 1;
        } else return -1;
    }

    /**
     * Longest arranged sequence
     *
     * @param board the entities to be passed as argument for which we calculate the longest sequence of in-order tiles
     * @return the lenght of the largest ordered substring
     */
    private static int longestArangedSequence(Board board) {
        Scanner scanner = new Scanner(board.toString().replaceAll("[\\[\\],]", ""));
        int largestCount = 0;
        int counter = 0;
        int currentValue, nextValue;
        currentValue = scanner.nextInt();       // we assume that there is at least one int in the input, otherwise this will fail
        while (scanner.hasNext()) {
            nextValue = scanner.nextInt();
            if (currentValue + 1 == nextValue && currentValue != 0) {
                counter++;
            } else {
                largestCount = (largestCount < counter) ? counter : largestCount;
                counter = 0;
            }
            currentValue = nextValue;
        }
        scanner.close();
        return -largestCount + board.getDepth(); // we return the negative value since we are looking for a lowest heuristic (a greater negative will win)
    }


}
