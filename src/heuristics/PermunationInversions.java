package heuristics;

import entities.Board;

import java.util.Arrays;
import java.util.Comparator;

public class PermunationInversions implements Comparator<Board> {
    @Override
    public int compare(Board o1, Board o2) {
        if (permunationInversions(o1) == permunationInversions(o2)) {
            return o1.compareWeights(o2);
        } else if (permunationInversions(o1) > permunationInversions(o2)) {   // smaller heuristics wins
            return 1;
        } else return -1;
    }

    /**
     * Sum of permutation inversions
     * For each tile, count how many tiles on its right are smaller, or should be on its left side in the goal state
     *
     * @param board the entities to be passed as argument for which we calculate the total number of permutation inversions
     * @return the sum of the inversions for each of the tiles
     */
    private int permunationInversions(Board board) {
        String boardString = board.toString().replaceAll("[\\[\\]\\s]", "");
        int[] boardArray = Arrays.stream(boardString.split(",")).mapToInt(Integer::parseInt).toArray();
        int inversions = 0;
        for (int i = 0; i < boardArray.length; i++) {
            int current = boardArray[i];
            if (current == 0) continue;
            for (int j = i + 1; j < boardArray.length; j++) {
                if (boardArray[j] < current && boardArray[j] != 0) {
                    inversions++;
                }
            }
        }
        return inversions + board.getDepth(); // the depth is only used as g(n) with A*;
    }

}
