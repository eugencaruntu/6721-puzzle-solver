package heuristics;

import entities.Board;
import entities.Tile;

import java.util.Comparator;

public class ChebyshevDistance implements Comparator<Board> {

    @Override
    public int compare(Board o1, Board o2) {
        if (chebyshevDistance(o1) == chebyshevDistance(o2)) {
            return o1.compareWeights(o2);
        } else if (chebyshevDistance(o1) > chebyshevDistance(o2)) {   // smaller heuristics wins
            return 1;
        } else return -1;
    }

    /**
     * Chebyshev Distance calculated as sum of largest absolute distances between coordinates of each tile
     *
     * @param board the entities to be passed as argument for which we calculate the Chebyshev Distance
     * @return the sum representing the total Chebyshev Distance
     */
    private static int chebyshevDistance(Board board) {
        int distance = 0;
        for (Tile[] row : board.getBoard()) {
            for (Tile tile : row) {
                int value = tile.getValue();
                if (value != 0) {
                    int r0 = tile.getRowIdx();                              // current row index
                    int c0 = tile.getColIdx();                              // current column index
                    int r1 = (value - 1) / board.getColumns();              // goal row index
                    int c1 = (value - 1) % board.getColumns();              // goal column index
                    distance += Math.max(Math.abs(r1 - r0), Math.abs(c1 - c0));
                }
            }
        }
        return distance + board.getDepth(); // the depth is only used as g(n) with A*
    }

}
