package heuristics;

import entities.Board;
import entities.Tile;

import java.util.Comparator;

public class HalfManhattanDistance implements Comparator<Board> {

    @Override
    public int compare(Board o1, Board o2) {
        if (halfManhattanDistance(o1) == halfManhattanDistance(o2)) {
            return o1.compareWeights(o2);
        } else if (halfManhattanDistance(o1) > halfManhattanDistance(o2)) {   // smaller heuristics wins
            return 1;
        } else return -1;
    }

    /**
     * Manhattan Distance calculated as sum of absolute distances between each coordinates of each tile
     *
     * @param board the entities to be passed as argument for which we calculate the Manhattan Distance
     * @return the sum representing the total Manhattan Distance
     */
    private static int halfManhattanDistance(Board board) {
        int distance = 0;
        for (Tile[] row : board.getBoard()) {
            for (Tile tile : row) {
                int value = tile.getValue();
                if (value != 0) {
                    int r0 = tile.getRowIdx();                              // current row index
                    int c0 = tile.getColIdx();                              // current column index
                    int r1 = (value - 1) / board.getColumns();              // goal row index
                    int c1 = (value - 1) % board.getColumns();              // goal column index
                    distance += Math.abs(r1 - r0) + Math.abs(c1 - c0);
                }
            }
        }
        return distance/2 + board.getDepth(); // the depth is only used as g(n) with A*
    }

}
