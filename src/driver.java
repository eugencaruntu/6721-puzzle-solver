import entities.Game;
import heuristics.*;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class driver {

    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("\nEnter the row count: ");
        int rows = Integer.parseInt(scanner.nextLine());

        System.out.println("\nEnter the column count: ");
        int columns = Integer.parseInt(scanner.nextLine());

        System.out.println("\nHere are some possible game inputs:" +
                "\nExamples for a 3x4 game" +
                "\n1 0 3 7 5 2 6 4 9 10 11 8" +
                "\n5 1 2 3 9 6 7 4 0 10 11 8" +
                "\n4 1 2 5 6 0 7 9 10 8 11 3" +
                "\n1 0 3 4 5 2 7 8 9 10 6 11" +
                "\n0 1 2 3 5 6 7 4 9 10 11 8" +
                "\nExamples for a 3x3 game" +
                "\n4 1 2 6 0 7 5 8 3" +
                "\n1 2 3 4 5 6 7 0 8" +
                "\n1 4 3 7 0 6 5 8 2" +
                "\nEnter the input for the board game in one line, using space as separator:");
        int[] input = Arrays.stream(scanner.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        testInput(rows, columns, input);

        System.out.println("\nThese are the possible strategies:");
        System.out.println("\n1. DFS (recommended to be played on 3x3 puzzle. It might not finish on 3x4" +
                "\n2. Iterative Deepening DFS (using iterative depth of 10)" +
                "\n3. BFS with following 4 heuristics: HammingDistance, HalfManhattanDistance, ChebyshevDistance, PermunationInversions" +
                "\n4. A* with following 3 admissible heuristics: HammingDistance, HalfManhattanDistance, ChebyshevDistance" +
                "\n5. BFS with a poorly informed heuristic for Longest ordered sub-sequence" +
                "\nEnter the number corresponding to the option you wish to play: ");

        int option = Integer.parseInt(scanner.nextLine());
        if (option < 1 || option > 5) {
            System.out.println("your input was invalid. Please restart the game.");
            System.exit(1);
        }

        playGame(rows, columns, input, option);
        scanner.close();
    }

    private static void playGame(int rows, int columns, int[] input, int option) {
        Game game;
        System.out.println("\n Playing the game... ");

        switch (option) {
            case (1):   // Play DFS
                try {
                    game = new Game(rows, columns, input);
                    game.DFS();
                } catch (OutOfMemoryError error) {
                    System.out.println("No solution found. DFS cant continue to play this game.");
                    System.exit(1);
                }
                break;
            case (2):   // Play Iterative Deepening DFS
                game = new Game(rows, columns, input);
                game.DFSIterativeDeepening(10);
                break;
            case (3):   // Play BFS with all heuristics
                Comparator[] comparatorsBFS = {new HammingDistance(), new HalfManhattanDistance(), new ChebyshevDistance(), new PermunationInversions()};
                for (Comparator comparator : comparatorsBFS) {
                    game = new Game(rows, columns, input);
                    game.BSF(comparator);
                }
                break;
            case (4):   // Play A* with admissible heuristics
                Comparator[] comparatorsAs = {new HammingDistance(), new HalfManhattanDistance(), new ChebyshevDistance()};
                for (Comparator comparator : comparatorsAs) {
                    game = new Game(rows, columns, input);
                    game.As(comparator);
                }
                break;
            case (5):   // Play BFS with experimental heuristic of Longest ordered sub-sequence
                game = new Game(rows, columns, input);
                game.BSF(new LongestArrangedSequence());
                break;
        }

    }

    /**
     * Some minimalistic checks to ensure user input is correct.
     * Because of the relaxed rules allowing diagonal move, the check for odd permutation inversions is not needed
     *
     * @param rows    the row count
     * @param columns the column count
     * @param input   the input
     */
    private static void testInput(int rows, int columns, int[] input) {
        // Count input elements and reject if not equal to (rows*columns), including the zero tile
        if (input.length != (rows * columns)) {
            System.out.println("The input size is incorrect for the number of rows and columns entered. Please restart the game.");
            System.exit(1);
        }

        // reject input that is missing numbers
        int[] test = Arrays.copyOf(input, input.length);
        Arrays.sort(test);
        for (int i = 1; i < test.length; i++) {
            if (test[i] != (test[i - 1] + 1)) {
                System.out.println("The numbers in the input are not a successive sequence once sorted. Please restart the game.");
                System.exit(1);
            }
        }
    }


}
