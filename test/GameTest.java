import entities.Game;
import heuristics.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.Scanner;

class GameTest {
    private String inputString;
    private Game game;

    @BeforeEach
    void setUp() {
        // 3 x 4
        inputString = "1 0 3 7 5 2 6 4 9 10 11 8";
//        inputString = "5 1 2 3 9 6 7 4 0 10 11 8";
//        inputString = "4 1 2 5 6 0 7 9 10 8 11 3"; // AS will fail
//        inputString = "1 0 3 7 5 2 6 4 9 10 11 8";
//        inputString = "1 0 3 4 5 2 7 8 9 10 6 11";
//        inputString = "0 1 2 3 5 6 7 4 9 10 11 8";

        // 3 x 3
//        inputString = "4 1 2 6 0 7 5 8 3";
//        inputString = "1 2 3 4 5 6 7 0 8";
//        inputString = "1 4 3 7 0 6 5 8 2";

        System.setIn(new ByteArrayInputStream(inputString.getBytes()));
        Scanner scanner = new Scanner(System.in);
        int[] input = Arrays.stream(scanner.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();

        game = new Game(3, 4, input);

    }

    @AfterEach
    void tearDown() {
        System.setIn(System.in);
    }

    @Test
    void DFS() {
        game.DFS();
    }

    @Test
    void DFSIterativeDeepening() {
        game.DFSIterativeDeepening(10);
    }

    @Test
    void BSFLongestArrangedSequence() {
        game.BSF(new LongestArrangedSequence());
    }

    @Test
    void BSFPermunationInversions() {
        game.BSF(new PermunationInversions());
    }

    @Test
    void BSFHammingDistance() {
        game.BSF(new HammingDistance());
    }

    @Test
    void BSFHalfManhattanDistance() {
        game.BSF(new HalfManhattanDistance());
    }

    @Test
    void BSFChebyshevDistance() {
        game.BSF(new ChebyshevDistance());
    }

    @Test
    void asHammingDistance() {
        game.As(new HammingDistance());
    }

    @Test
    void asHalfManhattanDistance() {
        game.As(new HalfManhattanDistance());
    }

    @Test
    void asChebyshevDistance() {
        game.As(new ChebyshevDistance());
    }
}