### Algorithms and heuristics implementation for solving variable size board puzzle game having relaxed rules
#### COMP6721
Eugen Caruntu | Student ID: 29077103

####Instructions
- When using Eclipse, make a new Java project and import the files into it(directly from zip or from expanded folder)
- To run the application launch the main() in driver.java
- Follow the console requests to play the game.

####NOTE: a move UP represents that the zeroTile moves up and the tile above it will move down. This implementation uses *Definition 2*.

